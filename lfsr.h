#ifndef LFSR_H
#define LFSR_H
#include <cstdint>

/**
 * @brief Класс реализующий 32битный сдвиговый регистр с обратной связью
 */
class LFSR
{
public:
    typedef uint_least32_t reg;

    typedef int8_t result_type;

    LFSR(reg shift_register = default_register, reg mask = default_mask);

    result_type operator()();

    void set_seed(reg new_seed);

    result_type min() { return 0; };

    result_type max() { return 1; };

private:

    static const reg default_register = 0x143a'3221;

    static const reg default_mask = 0x8000'0000;

    reg shift_register,
        mask;

};
#endif // LFSR_H
