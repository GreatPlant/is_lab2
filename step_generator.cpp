#include "step_generator.h"

StepGenerator::StepGenerator() {
    lfsr1 = LFSR(std::random_device()(), 0x8000'0057);
    lfsr2 = LFSR(std::random_device()(), 0x8000'08E1);
    lfsr3 = LFSR(std::random_device()(), 0x8000'0EA6);
}

StepGenerator::result_type StepGenerator::operator()() {
    return lfsr1() ? (lfsr2() | lfsr3()) : lfsr3();
}

void StepGenerator::discard() {
    lfsr1.set_seed(std::random_device()());
    lfsr2.set_seed(std::random_device()());
    lfsr3.set_seed(std::random_device()());
}
