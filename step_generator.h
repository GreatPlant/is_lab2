#ifndef RANDOMGENERATOR_H
#define RANDOMGENERATOR_H
#include "lfsr.h"
#include "basegenerator.h"

class StepGenerator : public IGenerator
{
public:
    StepGenerator();

    result_type min() const override { return 0; };

    result_type max() const override { return 1; };

    result_type operator()() override;

    void discard() override;
private:
    LFSR lfsr1, lfsr2, lfsr3;
};

#endif // RANDOMGENERATOR_H
