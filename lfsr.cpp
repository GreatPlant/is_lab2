#include "lfsr.h"

LFSR::LFSR(reg shift_register, reg mask) : shift_register(shift_register),
                                           mask(mask) {};

LFSR::result_type LFSR::operator()() {
    if(shift_register & 0x00000001) {
        shift_register = ((shift_register ^ mask) >> 1) | 0x80000000;
        return 1;
    } else {
        shift_register >>= 1;
        return 0;
    }
}


void LFSR::set_seed(reg new_seed) {
    shift_register = new_seed;
}
