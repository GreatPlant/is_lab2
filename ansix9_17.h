#ifndef ANSIX9_17_H
#define ANSIX9_17_H
#include <limits>
#include <chrono>
#include "des/des3.h"
#include "basegenerator.h"

using namespace std::chrono;

class ANSIX9_17 : public IGenerator
{
public:
    ANSIX9_17();

    result_type min() const { return 0; };

    result_type max() const { return std::numeric_limits<result_type>::max(); };

    result_type operator()();;

    void discard();
private:
    DES3 des3_1, des3_2, des3_3;
    ui64 d,
         temp,
         s_current,
         x_current;
};

#endif // ANSIX9_17_H
