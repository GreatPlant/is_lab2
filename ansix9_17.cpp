#include "ansix9_17.h"

ANSIX9_17::ANSIX9_17() : des3_1(1, 1, 1), des3_2(1, 1, 1), des3_3(1, 1, 1){
    std::random_device rd;
    ui64 k1 = rd(),
         k2 = rd();

    des3_1 = DES3(k1, k2, k1);
    des3_1 = DES3(k1, k2, k1);
    des3_1 = DES3(k1, k2, k1);

    s_current = rd();
}


ANSIX9_17::result_type ANSIX9_17::operator()(){
    d = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
    temp = des3_1.encrypt(d);
    x_current = des3_2.encrypt(temp ^ s_current);
    s_current = des3_3.encrypt(x_current ^ temp);
    return x_current;
}

void ANSIX9_17::discard() {
    s_current = std::random_device()();
}
