#include <iostream>
#include <map>
#include <chrono>
#include "tests/include/tests.h"
#include "tests/include/io.h"

#include "basegenerator.h"
#include "lcg.h"
#include "step_generator.h"
#include "ansix9_17.h"


LCG lcg((unsigned int)std::random_device()());

StepGenerator sg;

ANSIX9_17 ansi_gen;

std::map<int, IGenerator&> gens = {{ 1, lcg }, { 2, sg }, {3, ansi_gen }};

IGenerator& select_generator() {
    int choosen = 1;
    fmt::print("Choose generator:\n"
               "\tLinear congruental: 1\n"
               "\tStep: 2\n"
               "\tANSI X9.17: 3\n"
          "Your choose: ");
    std::cin >> choosen;
    if(choosen > (int)gens.size()) {
        fmt::print("Wrong input! Selected first\n");
    }
    return gens.find(choosen)->second;
}

int main()
{
    std::uniform_int_distribution<uint8_t> uid(0, 1);
    sequence epsilon;
    int choosen = 0;
    do {
        fmt::print("Choose function:\n"
                        "\tGenerate: 1\t\t"         "Load from file: 2\n"
                        "\tOut on screen: 3\t"      "Out to file: 4\n"
                        "\tFreq test: 5\t\t"        "Block test: 6\n"
                        "\tDeviation test: 7\t"     "All tests: 8\n"
                   "Enter 0 for exit\n"
                   "Your choose: ");
        std::cin >> choosen;
        switch (choosen) {
            case 1:
            {
                tst::generate<IGenerator&, uint8_t>(epsilon,
                                                    select_generator(),
                                                    uid);
                break;
            }
            case 2:
                loadFromFile(epsilon);
                break;
            case 3:
                output(std::cout, epsilon, " ");
                break;
            case 4:
                outToFile(epsilon);
                break;
            case 5:
                print_result("Freq test", tst::freq_test(epsilon));
                break;
            case 6:
                print_result("Block test", tst::block_freq_test(epsilon));
                break;
            case 7:
                print_d_test(tst::random_deviation_test(epsilon));
                break;
            case 8:
                print_result("Freq test", tst::freq_test(epsilon));
                print_result("Block test", tst::block_freq_test(epsilon));
                print_d_test(tst::random_deviation_test(epsilon));
                break;
            case 0:
                fmt::print("EXIT\n");
                break;
            default:
                fmt::print("Wrong! Try again\n");
        }
    } while(choosen);
    return std::cout.rdstate();
}
